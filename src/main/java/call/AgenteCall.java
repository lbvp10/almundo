package call;

/**
 * Clase que maneja las propiedades basicas de un agente de Calle center
 * 
 * @author lbvp10
 *
 */
public class AgenteCall extends Persona {

    /**
     * Estado actual del agente segun la llamada
     */
    private EstadoLLamada estadoLLamada = EstadoLLamada.DISPONIBLE;

    /**
     * Prioridad de un agente en la cola de agentes disponibles
     */
    private RolAgenteCall rolAgenteCall;

    /**
     * Objeto observable para la implementacion del patron Observador con el fin
     * de notificar al dispatcher un cambio de estado
     */
    private AgenteCallObservable observable = new AgenteCallObservable();

    public AgenteCall(RolAgenteCall rolAgenteCall, Long id, String nombres) {
	super(id, nombres);
	this.rolAgenteCall = rolAgenteCall;
    }

    public void recibirLlamada(LLamada llamada) {
	this.setEstadoLLamada(EstadoLLamada.OCUPADO);
    }

    public void finalizarLlamada(LLamada llamada) {
	setEstadoLLamada(EstadoLLamada.DISPONIBLE);
    }

    /**
     * @return the estadoLLamada
     */
    public EstadoLLamada getEstadoLLamada() {
	return estadoLLamada;
    }

    /**
     * @param estadoLLamada
     *            the estadoLLamada to set
     */
    public void setEstadoLLamada(EstadoLLamada estadoLLamada) {
	this.estadoLLamada = estadoLLamada;
	if (EstadoLLamada.DISPONIBLE.equals(estadoLLamada)) {
	    synchronized (observable) {
		observable.setChanged();
		observable.notifyObservers(this);
	    }
	}
    }

    /**
     * @return the observable
     */
    public AgenteCallObservable getObservable() {
	return observable;
    }

    /**
     * @param observable
     *            the observable to set
     */
    public void setObservable(AgenteCallObservable observable) {
	this.observable = observable;
    }

    /**
     * @return the rolAgenteCall
     */
    public RolAgenteCall getRolAgenteCall() {
	return rolAgenteCall;
    }

    /**
     * @param rolAgenteCall
     *            the rolAgenteCall to set
     */
    public void setRolAgenteCall(RolAgenteCall rolAgenteCall) {
	this.rolAgenteCall = rolAgenteCall;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	AgenteCall other = (AgenteCall) obj;
	if (id != other.id)
	    return false;
	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return super.toString();
    }

}
