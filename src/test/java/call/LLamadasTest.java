package call;

import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;

import junit.framework.Assert;

public class LLamadasTest {

	/**
	 * Prueba que indica como se registran los agentes en la lista de agentes
	 */
	@Test
	public void testRegistrarAgente() {
	    
	    		    	
		AgenteCall agenteCall = new AgenteCall(RolAgenteCall.SUPERVISOR, 1L, "supervisor");
		AgenteCall agenteCall2 = new AgenteCall(RolAgenteCall.OPERADOR, 2L, "op");
		AgenteCall agenteCall3 = new AgenteCall(RolAgenteCall.DIRECTOR, 3L, "dir");
		Dispatcher dis = Dispatcher.getInstancia();
		dis.registrarAgente(agenteCall);
		dis.registrarAgente(agenteCall2);
		dis.registrarAgente(agenteCall3);

		System.out.println(dis.getAgentes());

		Assert.assertTrue(dis.getAgentes().first().getId().equals(2L));
	}

	/**
	 * Pruba para determinar la concurrencia de las llamadas Maximo diez al
	 * tiempo
	 * 
	 * @throws InterruptedException
	 */

	@Test
	public void testDispatchCall() throws InterruptedException {
		Dispatcher dis = Dispatcher.getInstancia();
		// registro de agentes
		dis.registrarAgente(new AgenteCall(RolAgenteCall.SUPERVISOR, 10L, "supervisor"));
		dis.registrarAgente(new AgenteCall(RolAgenteCall.OPERADOR, 12L, "operador"));
		dis.registrarAgente(new AgenteCall(RolAgenteCall.DIRECTOR, 11L, "dir"));

		// ingreso de las llamadas
		dis.dispatchCall(new LLamada(1L));
		dis.dispatchCall(new LLamada(2L));
		dis.dispatchCall(new LLamada(3L));
		dis.dispatchCall(new LLamada(4L));
		dis.dispatchCall(new LLamada(5L));
		
		// registro de un agente mas
		dis.registrarAgente(new AgenteCall(RolAgenteCall.OPERADOR, 20L, "operador22"));
		
		// ingreso de mas llamadas
		dis.dispatchCall(new LLamada(6L));
		dis.dispatchCall(new LLamada(7L));
		dis.dispatchCall(new LLamada(8L));
		dis.dispatchCall(new LLamada(9L));
		dis.dispatchCall(new LLamada(10L));

		// Espera
		for (int i = 0; i < 30; i++) {
			Thread.sleep(1000L);
		}

		Assert.assertTrue(!dis.hayLlamadaEnEspera());
	}

}
