/**
 * 
 */
package call;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

import javax.swing.plaf.synth.SynthSpinnerUI;

/**
 * Clase encargada de manejar las colas de llegada, espera, asignacion de
 * agentes
 * 
 * @author lbvp10
 *
 */

@SuppressWarnings("unused")
public class Dispatcher implements Observer {

    private static final Logger LOGGER = Logger.getLogger(Dispatcher.class.getName());

    /**
     * Lista de agentes disponibles en el sistema
     */
    private SortedSet<AgenteCall> agentes;

    /**
     * Cola donde se registran todas las llamadas que ingresan al sistema
     */
    private Queue<LLamada> colaLlegada;

    /**
     * Cola donde ingresan las llamadas que no pueden ser atendidas
     * inmediatamente
     */
    private Queue<LLamada> colaEspera;

    // pool de hilos
    private ExecutorService poolHilos;

    // Instacia de la clase para la implementacion del Sigleton
    private static Dispatcher dispatcher;

    /**
     * Obtener una sola instancia de esta clase
     */
    public static Dispatcher getInstancia() {
	if (dispatcher == null) {
	    // Hilo seguro
	    synchronized (Dispatcher.class) {
		if (dispatcher == null) {
		    dispatcher = new Dispatcher();
		}
	    }
	}
	return dispatcher;
    }

    private Dispatcher() {

	Comparator<AgenteCall> comparator = new Comparator<AgenteCall>() {
	    @Override
	    public int compare(AgenteCall o1, AgenteCall o2) {
		int resultado = o1.getRolAgenteCall().getCode().compareTo(o2.getRolAgenteCall().getCode());
		if (resultado != 0) {
		    return resultado;
		}
		resultado = o1.getId().compareTo(o2.getId());
		return resultado;
	    }
	};
	agentes = new TreeSet<AgenteCall>(comparator);
	colaEspera = new LinkedList<LLamada>();
	colaLlegada = new LinkedList<LLamada>();

	/**
	 * Crea un pool de 10 hilos, las tareas que lleguen despues de 10 deben
	 * esperar que un hilo se desocupe
	 */
	poolHilos = Executors.newFixedThreadPool(10);
    }

    /**
     * Añade los agentes a la lista de agentes disponibles
     * 
     * @param agente
     */
    public void registrarAgente(AgenteCall agente) {
	agente.getObservable().addObserver(this);
	agentes.add(agente);
    }

    /**
     * Metodo que recepciona una llamada y la encola en llegada
     * 
     * @param llamada:
     *            datos de la llamada
     */
    public void dispatchCall(LLamada llamada) {
	//Se genera una duracion de llamada aleatoria, entre un rango de 5 y 10 segundos
	llamada.setDuracionLlamada(ThreadLocalRandom.current().nextInt(5, 11));
	colaLlegada.offer(llamada);
	dispatchCall(ColaLLamada.LLEGADA);
    }

    /**
     * Asigna un agente a la llamada o la encola en la cola de espera segun sea
     * el caso
     * 
     * @param colaLlamada:De
     *            que cola se saca la llamada a realizar
     */
    private synchronized void dispatchCall(ColaLLamada colaLlamada) {

	LLamada llamada = obtenerLLamadaSegunLaCola(colaLlamada);
	if (llamada == null) {
	    LOGGER.info(":::::::::No hay llamadas para realizar");
	    return;
	}

	// Obtener el agente disponible
	AgenteCall agente = obtenerAgenteDisponibleConMayorPrioridad();
	if (agente == null) {
	    // LOGGER.info(":::::::::No hay agentes disponibles, se envia a la
	    // cola de espera.." + llamada.getId());
	    colaEspera.offer(llamada);
	    return;
	}
	// LOGGER.info("Para la llamada id:" + llamada.getId() + " El agente
	// disponible es" + agente.getId());
	// realizar la llamada con un numero maximo de 10 hilos concurrentes
	Runnable manejador = new ManejadorLlamada(llamada, agente);
	poolHilos.execute(manejador);

    }

    /**
     * Segun la cola que se indique se obtiene la primera llamada que llego
     * 
     * @param colaLlamada:Cola
     *            de donde se obtiene la llamada
     * @return dtos de la llamada
     */
    private LLamada obtenerLLamadaSegunLaCola(ColaLLamada colaLlamada) {
	LLamada llamada = null;
	switch (colaLlamada) {
	case ESPERA:
	    llamada = colaEspera.poll();
	    break;
	case LLEGADA:
	    llamada = colaLlegada.poll();
	    break;
	}
	return llamada;
    }

    /**
     * Obtiene el primer agente disponible con mayor prioridad
     * 
     * @return agente
     */
    private AgenteCall obtenerAgenteDisponibleConMayorPrioridad() {
	AgenteCall agente = null;
	// Si no hay agentes en la lista de agentes
	if (agentes == null || agentes.isEmpty()) {
	    return agente;
	}
	// Obtener del set ordenado el primer agente disponible
	agente = agentes.stream().filter(a -> EstadoLLamada.DISPONIBLE.equals(a.getEstadoLLamada())).findFirst().orElse(null);

	return agente;

    }

    /**
     * Implementacion de patron observador para notificar que un agente cambio
     * el estado a DISPONIBLE
     */

    @Override
    public void update(Observable arg0, Object arg1) {
	if (colaEspera.peek() == null && colaLlegada.peek() == null) {
	    return;
	}
	LOGGER.info(":::::::::El agente:" + ((AgenteCall) arg1).getId() + " indica que esta disponible");
	dispatchCall(ColaLLamada.ESPERA);
    }

    /**
     * @return the agentes
     */
    public SortedSet<AgenteCall> getAgentes() {
	return agentes;
    }

    /**
     * @return the poolHilos
     */
    public ExecutorService getPoolHilos() {
	return poolHilos;
    }

    public boolean hayLlamadaEnEspera() {
	if (colaEspera.peek() == null && colaLlegada.peek() == null) {
	    return false;
	}
	return true;
    }

}
