package call;

/**
 * Roles de un agente de cal center
 * 
 * @author lbvp10
 *
 */
public enum RolAgenteCall {

	OPERADOR(1, "Operador"), SUPERVISOR(2, "Supervisor"), DIRECTOR(3, "diretor");

	private Integer code;
	private String value;

	RolAgenteCall(int code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}
