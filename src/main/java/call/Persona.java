package call;

/**
 * Clase que maneja la informacion basica de una persona
 * 
 * @author lbvp10
 *
 */
public abstract class Persona {

	/**
	 * Identificador unico
	 */
	protected Long id;

	/**
	 * Nombres del agente
	 */
	protected String nombres;
	
	

	public Persona(Long id, String nombres) {
		super();
		this.id = id;
		this.nombres = nombres;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombres
	 */
	public String getNombres() {
		return nombres;
	}

	/**
	 * @param nombres
	 *            the nombres to set
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Persona [id=" + id + ", nombres=" + nombres + "]";
	}
	

}
