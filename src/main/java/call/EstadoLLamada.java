package call;

/**
 * Estados que puede manejar un agente de call center
 * 
 * @author lbvp10
 *
 */

public enum EstadoLLamada {

	DISPONIBLE, OCUPADO, AUSENTE
}
