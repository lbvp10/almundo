package call;

import java.util.logging.Logger;

/**
 * Hilo que se lanza en una llamada
 * 
 * @author lbvp10
 *
 */
public class ManejadorLlamada implements Runnable {

	private LLamada llamada;

	private AgenteCall agenteCall;

	private static final Logger LOGGER = Logger.getLogger(ManejadorLlamada.class.getName());

	public ManejadorLlamada(LLamada llamada, AgenteCall agenteCall) {
		this.llamada = llamada;
		this.agenteCall = agenteCall;
		this.agenteCall.setEstadoLLamada(EstadoLLamada.OCUPADO);

		LOGGER.info("-----------------inicializando llamada con id:" + llamada.getId() + " Duracion:"
				+ llamada.getDuracionLlamada() + " Agente:" + agenteCall.getId());
	}

	@Override
	public void run() {
		try {
		    	agenteCall.recibirLlamada(llamada);
			Thread.sleep(llamada.getDuracionLlamada() * 1000);
		} catch (InterruptedException e) {
			LOGGER.info("Error para la llamada:" + llamada.getId());
		}
		
		LOGGER.info("::::::Finalizo llamada con id:" + llamada.getId());
		
		agenteCall.finalizarLlamada(llamada);

	}

}
