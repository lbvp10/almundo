package call;

import java.time.LocalDateTime;

/**
 * Calse que maneja la informacion de una llamada Inbound
 * 
 * @author lbvp10
 *
 */
public class LLamada {

    private Long id;

    private LocalDateTime fecha;

    private String numeroTelefonoInBound;

    private AgenteCall ageneteAtiende;

    private int duracionLlamada;

    public LLamada(Long id) {
	super();
	this.id = id;
    }
    

    /**
     * @return the id
     */
    public Long getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
	this.id = id;
    }

    /**
     * @return the fecha
     */
    public LocalDateTime getFecha() {
	return fecha;
    }

    /**
     * @param fecha
     *            the fecha to set
     */
    public void setFecha(LocalDateTime fecha) {
	this.fecha = fecha;
    }

    /**
     * @return the ageneteAtiende
     */
    public AgenteCall getAgeneteAtiende() {
	return ageneteAtiende;
    }

    /**
     * @param ageneteAtiende
     *            the ageneteAtiende to set
     */
    public void setAgeneteAtiende(AgenteCall ageneteAtiende) {
	this.ageneteAtiende = ageneteAtiende;
    }

    /**
     * @return the numeroTelefonoInBound
     */
    public String getNumeroTelefonoInBound() {
	return numeroTelefonoInBound;
    }

    /**
     * @param numeroTelefonoInBound
     *            the numeroTelefonoInBound to set
     */
    public void setNumeroTelefonoInBound(String numeroTelefonoInBound) {
	this.numeroTelefonoInBound = numeroTelefonoInBound;
    }

    /**
     * @return the duracionLlamada
     */
    public int getDuracionLlamada() {
	return duracionLlamada;
    }

    /**
     * @param duracionLlamada
     *            the duracionLlamada to set
     */
    public void setDuracionLlamada(int duracionLlamada) {
	this.duracionLlamada = duracionLlamada;
    }

}
