package call;

import java.util.Observable;

/**
 * Clase Observable para la implementacion del patron Observador
 * @author lbvp10
 *
 */
public class AgenteCallObservable extends Observable {
	@Override
	public synchronized void setChanged() {
		super.setChanged();
	}

}